##### Metrics
The project exposes metrics on /metrics endpoint

To run the nodejs application:

    npm install 
    node app/server.js

To build the project:

    docker build -t repo-name/image-name:image-tag .
    docker push repo-name/image-name:image-tag

To create secret key for docker repo credentials

    kubectl create secret docker-registry my-registry-key --docker-server=https://index.docker.io/v1/ --docker-username=vijay815 --docker-password=xxxxx